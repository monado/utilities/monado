Adds a initial SteamVR driver state tracker and target that produces a SteamVR
plugin that enables any Monado hardware driver to be used in SteamVR. This is
the initial upstreaming of this code and has some limitations, like only having
working input when emulating a Index controller.
